Identity

C 1 0 0
{
  R -90 1.0 0.0 0.0
  S .5 .5 1
  l-cylindre
}

C 0 1 0
{
  T .5 -.5 0
  {
    S .2 1 .2
    cube
  }
  T -1 0 0
  {
    S .2 1 .2
    cube
  }
}

C 1 1 0
{
  T 0 1.5 0
  S .5 .5 .5
  sphere
}
