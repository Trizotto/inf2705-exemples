// "#version 4xx\n#define NUANCEUR_...\n" doit être ajouté en préambule au chargement de chaque nuanceur
////////////////////////////////////////////////////////////////////////////////
#if defined(NUANCEUR_SOMMETS)

uniform mat4 matrModel;
uniform mat4 matrVisu;
uniform mat4 matrProj;
uniform mat3 matrNormale;
uniform vec4 lumiPosition;

layout(location=0) in vec4 Vertex;
layout(location=2) in vec3 Normal;

out Attribs {
    vec3 normale, lumiDir;
} AttribsOut;

void main( void )
{
    // appliquer la transformation standard du sommet
    gl_Position = matrProj * matrVisu * matrModel * Vertex;

    // calculer la normale qui sera interpolée pour le nuanceur de fragments
    AttribsOut.normale = matrNormale * Normal;

    // calculer la position du sommet dans le repère de la caméra
    vec3 pos = vec3( matrVisu * matrModel * Vertex );

    // calculer le vecteur de la direction de la lumière (lumière positionnelle)
    AttribsOut.lumiDir = ( matrVisu * lumiPosition ).xyz - pos;
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_FRAGMENTS)

uniform sampler1D laTexturePalette;

in Attribs {
    vec3 normale, lumiDir;
} AttribsIn;

out vec4 FragColor;

void main( void )
{
    vec3 N = normalize( AttribsIn.normale ); // vecteur normal
    vec3 L = normalize( AttribsIn.lumiDir ); // vecteur vers la source lumineuse

    float albedo = max( 0.0, dot( N, L ) );

    // créer des bandes régulièrement espacées avec floor (plus efficace)
    // float nbandes = 4.0;
    // vec4 couleur = vec4( vec3(albedo), 1. ); // teintes de gris
    // couleur = floor( couleur*nbandes )/nbandes;

    // créer des bandes régulièrement espacées avec texture
    vec4 couleur = texture( laTexturePalette, albedo );

    // seuiller chaque composante entre 0 et 1 et assigner la couleur finale du fragment
    FragColor = clamp( couleur, 0.0, 1.0 );
}
#endif
