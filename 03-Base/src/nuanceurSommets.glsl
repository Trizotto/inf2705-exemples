#version 410

uniform mat4 matrModel;
uniform mat4 matrVisu;
uniform mat4 matrProj;

layout(location=0) in vec4 Vertex;
layout(location=3) in vec4 Color;

// out gl_PerVertex // <-- déclaration implicite
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// };

out Attribs {
    vec4 couleurAvant;
    vec4 couleurArriere;
    vec4 pos; // position dans le repère de la caméra
} AttribsOut;

void main(void)
{
    // appliquer la transformation standard du sommet (P * V * M * sommet)
    gl_Position = matrProj * matrVisu * matrModel * Vertex;

    // on pourrait aussi faire comme ceci:
    // mat4 VM = matrVisu * matrModel;
    // mat4 PVM = matrProj * VM;
    // gl_Position = PVM * Vertex;

    // calculer position dans le repère de la caméra
    vec4 pos = matrVisu * matrModel * Vertex;
    AttribsOut.pos = pos;

    // assigner la couleur avant du sommet
    AttribsOut.couleurAvant = Color;
    // assigner la couleur arrière du sommet (l'inverse de la couleur avant; pourquoi pas?!)
    AttribsOut.couleurArriere = vec4( 1.0 - Color.rgb, Color.a );

    // À essayer :
    // AttribsOut.couleurArriere = vec4( 0.0, abs(0.5*pos.x), 0.0, 1.0 );
    // AttribsOut.couleurArriere = vec4( abs(pos.x), abs(pos.y), 0.0, 1.0 );
    // AttribsOut.couleurArriere = AttribsOut.couleurAvant.brga;

    //vec4 V = Vertex; V.xy *= 1.5;
    //gl_Position = matrProj * matrVisu * matrModel * V;
}
