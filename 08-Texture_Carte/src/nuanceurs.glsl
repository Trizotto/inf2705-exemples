// "#version 4xx\n#define NUANCEUR_...\n" doit être ajouté en préambule au chargement de chaque nuanceur
////////////////////////////////////////////////////////////////////////////////
#if defined(NUANCEUR_SOMMETS)
//#version 410

uniform mat4 matrModel, matrVisu, matrProj;

in vec4 Vertex;
out vec2 TexCoord;

void main( void )
{
    // appliquer la transformation standard du sommet
    gl_Position = matrProj * matrVisu * matrModel * Vertex;
    // TexCoord = gl_Position.xy; // de -1 à +1
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_FRAGMENTS)
//#version 410

uniform sampler2D laTextureCarte, laTextureBallon, laTextureBonhomme;

uniform int largeur, hauteur;

// in vec4 gl_FragCoord;
// in vec2 TexCoord;
out vec4 FragColor;

void main( void )
{
    // calculer les coordonnées de texture
    //vec2 texCoord = 0.5*(1.+TexCoord);
    vec2 texCoord = gl_FragCoord.xy / vec2(largeur,hauteur);

    // obtenir la température
    FragColor = texture( laTextureCarte, texCoord );

    // calculer les coordonnées de texture pour la petite image
#if 1
    vec2 imageCoord = gl_FragCoord.xy / vec2(50.,50.);
#elif 1
    vec2 imageCoord = texCoord * vec2( 12., 8. );
#elif 1
    vec2 imageCoord = ivec2(gl_FragCoord.xy)%50 / 50.;
#endif

    // modifier la couleur en appliquant les images
#if 1
    if ( FragColor.r != FragColor.b )
        FragColor = texture( FragColor.r > FragColor.b ? laTextureBallon : laTextureBonhomme, imageCoord );
#else
    if ( FragColor.r > FragColor.b )
        FragColor = texture( laTextureBallon, imageCoord );
    else if ( FragColor.b > FragColor.r )
        FragColor = texture( laTextureBonhomme, imageCoord );
#endif
}

#endif
