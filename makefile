all : clean liens

###
### Les touches
###

# créer les fichiers touches.txt (pour l'aide)
touches :
	for t in *-*/; do ( cd $$t; ls src/*.cpp 2>/dev/null 1>&2 && ../bin/touches.sh || true ); done
	chmod 644 */src/touches.txt
# voir l'utilisation des touches dans les exemples
touchesUtil :
	cat */src/touches.txt | sort -u

###
### Les fichiers communs
###

liens :
	for f in `echo srcCommun/*.h [0-9]*/*{.h,.cpp,.glsl}`; do ln -sf ../highlight.php $$f.php; git add $$f.php; done
liensrm :
	for f in `echo srcCommun/*.h [0-9]*/*{.h,.cpp,.glsl}`; do git rm $$f.php; done

communexem :
	for f in *-*/s*/inf2705-*.h; do diff -s srcCommun/`basename $$f` $$f || cp -p -i $$f srcCommun/`basename $$f`; done
communsrc :
	for f in *-*/s*/inf2705-*.h; do diff -s $$f srcCommun/`basename $$f` || cp -p -i srcCommun/`basename $$f` $$f; done

clean : ; rm -rf *-*/s??/{build-*,Debug,Release,x64,.vs,core}
realclean : clean ; rm -rf *-*/s??/packages

exe : ; for d in *-*/src/main.cpp; do ( cd `dirname $$d` && make exe ); done

run0 : ; (cd 00-*/src && make run)
run1 : ; (cd 01-*/src && make run)
run2 : ; (cd 02-*/src && make run)
run3 : ; (cd 03-*/src && make run)
run4 : ; (cd 04-*/src && make run)
run5 : ; (cd 05-*/src && make run)
run6 : ; (cd 06-*/src && make run)
run8 : ; (cd 08-Texture/src && make run)
run8f : ; (cd 08-TextureF*/src && make run)
run8m : ; (cd 08-TextureM*/src && make run)
