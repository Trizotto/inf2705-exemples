#ifndef __ETAT_H__
#define __ETAT_H__

#include <GL/glew.h>
#include <glm/glm.hpp>
#include "inf2705-Singleton.h"

//
// variables d'état
//
class Etat : public Singleton<Etat>
{
    SINGLETON_DECLARATION_CLASSE(Etat);
public:
    static glm::ivec2 poscour;    // la position courante de la souris
    static glm::ivec2 taillecour; // la taille courante de la fenêtre
    static int typeFlou;          // le type de flou souhaité
};

#endif
