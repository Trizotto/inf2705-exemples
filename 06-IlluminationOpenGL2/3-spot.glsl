/////////////////////////
// Nuanceur de sommets //
/////////////////////////

#version 130
out vec3 normale, lumidDir, obsVec;

void main()
{
    // calculer la normale qui sera interpolée pour le nuanceur de fragment
    normale = gl_NormalMatrix * gl_Normal;
    //normale = normalize( gl_NormalMatrix * gl_Normal );

    // calculer la position du sommet dans le repère de la caméra ("repère de l'observateur")
    vec3 pos = vec3( gl_ModelViewMatrix * gl_Vertex );

    // calculer le vecteur de la direction de la lumière (En OpenGl 2.x, gl_LightSource[i].position est déjà dans le repère de la caméra)
    lumidDir = vec3( gl_LightSource[0].position.xyz - pos );
    obsVec = (-pos); // vecteur qui pointe vers le (0,0,0), c'est-à-dire vers la caméra

    // appliquer la transformation standard du sommet (ModelView et Projection)
    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}

///////////////////////////
// Nuanceur de fragments //
///////////////////////////

#version 130
in vec3 normale, lumidDir, obsVec;

void main (void)
{
    // calculer la composante ambiante
    vec4 couleur = ( FrontMaterial.emission + FrontMaterial.ambient * LightModel.ambient ) +
    gl_FrontMaterial.ambient * gl_LightSource[0].ambient ;

    // calculer la direction de la lumière
    vec3 L = normalize( lumidDir );
    // calculer la direction du spot (En OpenGL 2.x, la direction est déjà dans le repère de la caméra)
    vec3 D = normalize( -gl_LightSource[0].spotDirection ); // (gl_LightSource[i].spotDirection est déjà dans le repère de la caméra)

    if ( dot(L, D) > cos(radians(LightSource[0].spotCutoff)) )
    {
        // calculer le vecteur normal
        vec3 N = normalize(normale);
        // calculer le produit scalaire pour la réflexion diffuse
        float NdotL = max( dot(N,L), 0.0 );
        if ( NdotL > 0.0 )
        {
            couleur += gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse * NdotL;

            vec3 O = normalize(obsVec);
            vec3 R = reflect( -L, N );
            couleur += gl_FrontMaterial.specular * gl_LightSource[0].specular * pow( max(dot(R, O), 0.0), gl_FrontMaterial.shininess );
        }
    }

    gl_FragColor = couleur;
}
