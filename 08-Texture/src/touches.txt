Touches possibles :
    q:  Quitter l'application
    v:  Recharger les nuanceurs
    g:  Permuter l'affichage en fil de fer ou plein
    ESPACE:  Mettre en pause ou reprendre l'animation
    i:  changer minFilter
    a:  changer magFilter
    t:  changer de mode d'affichage de la texture
    MOINS:  Incrémenter la distance de la caméra
    PLUS:  Décrémenter la distance de la caméra
    0:  Remettre les angles de la caméra à 0
    GAUCHE:  Augmenter theta
    DROITE:  Décrémenter theta
    HAUT:  Augmenter phi
    BAS:   Décrémenter phi
    r:  réinitiliaser la caméra
    s:  Sauvegarder une copie de la fenêtre dans un fichier
