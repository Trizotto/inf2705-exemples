#include "Etat.h"

SINGLETON_DECLARATION_CPP(Etat);

bool Etat::enmouvement = false;
GLdouble Etat::rtri = 0.;
GLdouble Etat::rquad = 0.;
int Etat::choix = 0;
GLuint Etat::maTextureAVendre = 0, Etat::maTextureEchiquier = 0;
GLenum Etat::magFilter = GL_LINEAR, Etat::minFilter = GL_LINEAR;
