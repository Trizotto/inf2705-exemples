#ifndef __ETAT_H__
#define __ETAT_H__

#include <GL/glew.h>
#include <glm/glm.hpp>
#include "inf2705-Singleton.h"

//
// variables d'état
//
class Etat : public Singleton<Etat>
{
    SINGLETON_DECLARATION_CLASSE(Etat);
public:
    static bool enmouvement;         // le modèle est en mouvement/rotation automatique ou non
    static GLint modele;             // le modèle à afficher
    static GLdouble rotationY;       // l'angle courant de rotation
    static GLdouble rotationIncr;    // l'incrément
    static int affichageStereo;      // type d'affichage: 0-mono, 1-stéréo anaglyphe
};

#endif
