# 05-NuanceurGeometrie
<a href="images/image.png"><img width=200px src="images/image.png"></a>

Comme exercice, enlever les deux <tt>Scale(1.5,1.5,1.5)</tt> dans
<tt>main.cpp</tt> et produire le même rendu visuel en modifiant les
nuanceurs.  
(Dans le nuanceur de géométrie...)<br> 
Pour référence, l'image de base (sans <tt>Scale</tt>) est <a href="images/imageBase.png"><img width=200px src="images/imageBase.png"></a>
