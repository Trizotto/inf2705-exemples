void afficherModele4()
{
    // « effacer » l'écran
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    // initiliaser la position du repère
    glLoadIdentity( );

    // se déplacer vers la gauche (et, accessoirement, vers l'arrière)
    glTranslatef( -1.5, 0.0, -6.0 );
    glRotatef( Etat::rtri, 0.0, 1.0, 0.0 );

    // afficher la pyramide (composée de plusieurs triangles)
    glBegin( GL_TRIANGLES );
      glColor3f( 1.0, 0.0, 0.0 );
      glVertex3f( 0.0, 1.0, 0.0 );
      glColor3f( 0.0, 1.0, 0.0 );
      glVertex3f( -1.0, -1.0, 1.0 );
      glColor3f( 0.0, 0.0, 1.0 );
      glVertex3f( 1.0, -1.0, 1.0 );
      glColor3f( 1.0, 0.0, 0.0 );
      glVertex3f( 0.0, 1.0, 0.0 );
      glColor3f( 0.0, 0.0, 1.0 );
      glVertex3f( 1.0, -1.0, 1.0 );
      glColor3f( 0.0, 1.0, 0.0 );
      glVertex3f( 1.0, -1.0, -1.0 );
      glColor3f( 1.0, 0.0, 0.0 );
      glVertex3f( 0.0, 1.0, 0.0 );
      glColor3f( 0.0, 1.0, 0.0 );
      glVertex3f( 1.0, -1.0, -1.0 );
      glColor3f( 0.0, 0.0, 1.0 );
      glVertex3f( -1.0, -1.0, -1.0 );
      glColor3f( 1.0, 0.0, 0.0 );
      glVertex3f( 0.0, 1.0, 0.0 );
      glColor3f( 0.0, 0.0, 1.0 );
      glVertex3f( -1.0, -1.0, -1.0 );
      glColor3f( 0.0, 1.0, 0.0 );
      glVertex3f( -1.0, -1.0, 1.0 );
    glEnd( );

    // déplacer le repère
    glLoadIdentity( ); // très mauvais de réinitiliaser ainsi, mais pratique!
    glTranslatef( 1.5, 0.0, -7.0 ); // vers la droite
    glRotatef( Etat::rquad, 1.0, 1.0, 1.0 ); // rotation de rquad degrés autour de (1,0,0)

    // afficher le cube  (composé de plusieurs quads)
    glBegin( GL_QUADS );
      glColor3f( 0.0, 1.0, 0.0 );
      glVertex3f( 1.0, 1.0, -1.0 );
      glVertex3f( -1.0, 1.0, -1.0 );
      glVertex3f( -1.0, 1.0, 1.0 );
      glVertex3f( 1.0, 1.0, 1.0 );
      glColor3f( 1.0, 0.5, 0.0 );
      glVertex3f( 1.0, -1.0, 1.0 );
      glVertex3f( -1.0, -1.0, 1.0 );
      glVertex3f( -1.0, -1.0, -1.0 );
      glVertex3f( 1.0, -1.0, -1.0 );
      glColor3f( 1.0, 0.0, 0.0 );
      glVertex3f( 1.0, 1.0, 1.0 );
      glVertex3f( -1.0, 1.0, 1.0 );
      glVertex3f( -1.0, -1.0, 1.0 );
      glVertex3f( 1.0, -1.0, 1.0 );
      glColor3f( 1.0, 1.0, 0.0 );
      glVertex3f( 1.0, -1.0, -1.0 );
      glVertex3f( -1.0, -1.0, -1.0 );
      glVertex3f( -1.0, 1.0, -1.0 );
      glVertex3f( 1.0, 1.0, -1.0 );
      glColor3f( 0.0, 0.0, 1.0 );
      glVertex3f( -1.0, 1.0, 1.0 );
      glVertex3f( -1.0, 1.0, -1.0 );
      glVertex3f( -1.0, -1.0, -1.0 );
      glVertex3f( -1.0, -1.0, 1.0 );
      glColor3f( 1.0, 0.0, 1.0 );
      glVertex3f( 1.0, 1.0, -1.0 );
      glVertex3f( 1.0, 1.0, 1.0 );
      glVertex3f( 1.0, -1.0, 1.0 );
      glVertex3f( 1.0, -1.0, -1.0 );
    glEnd( );
}
