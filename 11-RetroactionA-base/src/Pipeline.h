#ifndef __PIPELINE_H__
#define __PIPELINE_H__

// variables pour l'utilisation des nuanceurs
GLuint prog;
GLint locVertex;
GLint locColor;
GLint locmatrModel;
GLint locmatrVisu;
GLint locmatrProj;
GLint loctemps;

GLuint vao[1];   // un VAO
GLuint vbo[3];   // les VBO de cet exemple

// matrices du pipeline graphique
MatricePipeline matrModel, matrVisu, matrProj;

#endif
