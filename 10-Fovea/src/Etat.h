#ifndef __ETAT_H__
#define __ETAT_H__

#include <GL/glew.h>
#include <glm/glm.hpp>
#include "inf2705-Singleton.h"

//
// variables d'état
//
class Etat : public Singleton<Etat>
{
    SINGLETON_DECLARATION_CLASSE(Etat);
public:
    static bool enmouvement;       // le modèle est en mouvement/rotation automatique ou non
    static GLfloat temps;          // temps courant
    static const GLfloat dt;       // incrément de temps
    static glm::ivec2 taillecour;  // la taille courante de la fenêtre
};

#endif
