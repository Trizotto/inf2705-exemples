OOGL( function () {
    var oogl = new OOGL.Context('canvas', { alpha: false });
    oogl.enable(oogl.CULL_FACE);
    var arrays = new oogl.AttributeArrays(36);
    arrays.add3f([
	-1, 1, -1, -1, -1, -1, 1, -1, -1, -1, 1, -1, 1, -1, -1, 1, 1, -1,	// front
         1, 1, -1, 1, -1, -1, 1, -1, 1, 1, 1, -1, 1, -1, 1, 1, 1, 1,		// right
	-1, 1, 1, -1, 1, -1, 1, 1, -1, -1, 1, 1, 1, 1, -1, 1, 1, 1,		// up
	-1, 1, 1, -1, -1, 1, -1, -1, -1, -1, 1, 1, -1, -1, -1, -1, 1, -1,	// left
	-1, -1, -1, -1, -1, 1, 1, -1, 1, -1, -1, -1, 1, -1, 1, 1, -1, -1,	// down
	 1, 1, 1, 1, -1, 1, -1, -1, 1, 1, 1, 1, -1, -1, 1, -1, 1, 1,		// back
    ]);
    arrays.add2f([
	0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0,
	0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0,
	0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0,
	0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0,
	0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0,
	0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0,
    ]);
    arrays.bindAndPointer();
    new oogl.AsyncTexture('de.png', function () {
	var program = new oogl.AjaxProgram('box', ['in_Vertex', 'in_TexCoord'], function () {
	    program.use();
	    var roll = 0;
	    var pitch = 0;
	    var yaw = 0;
	    (new OOGL.RenderLoop(function () {
		oogl.clear(oogl.COLOR_BUFFER_BIT);
		program.uniform3f('Angle', pitch += 0.015, yaw += 0.01, roll += 0.02);
		arrays.drawTriangles();
		oogl.flush();
	    })).start();
	});
    });
});
