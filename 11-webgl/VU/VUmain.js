var gl;

function initGL(canvas)
{
   try {
      gl = canvas.getContext("experimental-webgl");
      gl.viewportWidth = canvas.width;
      gl.viewportHeight = canvas.height;
   } catch (e) {
   }
   if (!gl) {
      alert("Could not initialise WebGL, sorry :-(");
   }
}


function getShader(gl, id)
{
   var shaderScript = document.getElementById(id);
   if (!shaderScript)
   {
      return null;
   }

   var str = "";
   var k = shaderScript.firstChild;
   while (k) {
      if (k.nodeType == 3) // type text
      {
         str += k.textContent;
      }
      k = k.nextSibling;
   }

   var shader;
   if (shaderScript.type == "x-shader/x-fragment")
   {
      shader = gl.createShader(gl.FRAGMENT_SHADER);
   }
   else if (shaderScript.type == "x-shader/x-vertex")
   {
      shader = gl.createShader(gl.VERTEX_SHADER);
   }
   else
   {
      return null;
   }

   gl.shaderSource(shader, str);
   gl.compileShader(shader);

   if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
   {
      alert(gl.getShaderInfoLog(shader));
      return null;
   }

   return shader;
}


var shaderProgram;

function initShaders()
{
   var fragmentShader = getShader(gl, "shader-fs");
   var vertexShader = getShader(gl, "shader-vs");

   shaderProgram = gl.createProgram();
   gl.attachShader(shaderProgram, vertexShader);
   gl.attachShader(shaderProgram, fragmentShader);
   gl.linkProgram(shaderProgram);

   if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS))
   {
      alert("Could not initialise shaders");
   }

   gl.useProgram(shaderProgram);

   shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
   gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

   shaderProgram.vertexColorAttribute = gl.getAttribLocation(shaderProgram, "aVertexColor");
   gl.enableVertexAttribArray(shaderProgram.vertexColorAttribute);

   shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
   shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
}


var mvMatrix = mat4.create();
var mvMatrixStack = [];
var pMatrix = mat4.create();

function mvPushMatrix()
{
   var copy = mat4.create();
   mat4.set(mvMatrix, copy);
   mvMatrixStack.push(copy);
}

function mvPopMatrix()
{
   if (mvMatrixStack.length == 0)
   {
      throw "Invalid popMatrix!";
   }
   mvMatrix = mvMatrixStack.pop();
}


function setMatrixUniforms()
{
   gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
   gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
}


function degToRad(degrees)
{
   return degrees * Math.PI / 180;
}

var pyramidVertexPositionBuffer;
var pyramidVertexColorBuffer;

function initBuffers()
{
   pyramidVertexPositionBuffer = gl.createBuffer();
   gl.bindBuffer(gl.ARRAY_BUFFER, pyramidVertexPositionBuffer);
   gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(coo), gl.STATIC_DRAW);
   pyramidVertexPositionBuffer.itemSize = 3;
   pyramidVertexPositionBuffer.numItems = webglCount;

   pyramidVertexColorBuffer = gl.createBuffer();
   gl.bindBuffer(gl.ARRAY_BUFFER, pyramidVertexColorBuffer);
   gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(col), gl.STATIC_DRAW);
   pyramidVertexColorBuffer.itemSize = 4;
   pyramidVertexColorBuffer.numItems = webglCount;
}

var xSpeed = 0;
var ySpeed = 0;
var z = -5.0;

var currentlyPressedKeys = {};

function handleKeyDown(event)
{
   currentlyPressedKeys[event.keyCode] = true;
}

function handleKeyUp(event)
{
   currentlyPressedKeys[event.keyCode] = false;
}

function handleKeys()
{
   if (currentlyPressedKeys[33]) z -= 0.05; // Page Up
   if (currentlyPressedKeys[34]) z += 0.05; // Page Down
   if (currentlyPressedKeys[37]) ySpeed -= 1; // Left cursor key
   if (currentlyPressedKeys[39]) ySpeed += 1; // Right cursor key
   if (currentlyPressedKeys[38]) xSpeed -= 1; // Up cursor key
   if (currentlyPressedKeys[40]) xSpeed += 1; // Down cursor key
}

function drawScene()
{
   gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
   gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

   mat4.perspective(45, gl.viewportWidth / gl.viewportHeight, 0.1, 100.0, pMatrix);

   mat4.identity(mvMatrix);

   mat4.translate(mvMatrix, [0, 0.0, -8.0]);

   mvPushMatrix();
   mat4.rotate(mvMatrix, degToRad(xSpeed), [1, 0, 0]);
   mat4.rotate(mvMatrix, degToRad(ySpeed), [0, 1, 0]);

   gl.bindBuffer(gl.ARRAY_BUFFER, pyramidVertexPositionBuffer);
   gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, pyramidVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

   gl.bindBuffer(gl.ARRAY_BUFFER, pyramidVertexColorBuffer);
   gl.vertexAttribPointer(shaderProgram.vertexColorAttribute, pyramidVertexColorBuffer.itemSize, gl.FLOAT, false, 0, 0);

   setMatrixUniforms();
   gl.drawArrays(gl.TRIANGLES, 0, pyramidVertexPositionBuffer.numItems);

   mvPopMatrix();

   mat4.translate(mvMatrix, [3.0, 0.0, 0.0]);

}

function tick()
{
   requestAnimFrame(tick);
   handleKeys();
   drawScene();
}

function webGLStart()
{
   var canvas = document.getElementById("VUwebgl");
   initGL(canvas);
   initShaders()
   initBuffers();

   gl.clearColor(0.0, 0.0, 0.0, 1.0);
   gl.enable(gl.DEPTH_TEST);

   document.onkeydown = handleKeyDown;
   document.onkeyup = handleKeyUp;

   tick();
}
