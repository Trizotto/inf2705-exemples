// "#version 4xx\n#define NUANCEUR_...\n" doit être ajouté en préambule au chargement de chaque nuanceur
////////////////////////////////////////////////////////////////////////////////
#if defined(NUANCEUR_SOMMETS_RETROACTION)

uniform float dt;

layout(location=0) in vec4 Vertex;
layout(location=3) in vec4 Color;

out vec4 VertexMod;
out vec4 ColorMod;

void main( void )
{
    // construire une matrice de rotation
    mat4 Rotation = mat4(  cos(dt), sin(dt), 0.0, 0.0,
                           -sin(dt), cos(dt), 0.0, 0.0,
                           0.0, 0.0, 1.0, 0.0,
                           0.0, 0.0, 0.0, 1.0
                           );

    // appliquer cette rotation
    VertexMod = Rotation * Vertex;
    ColorMod = Rotation * (Color-0.5) + 0.5;
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_SOMMETS)

uniform mat4 matrModel;
uniform mat4 matrVisu;
uniform mat4 matrProj;

layout(location=0) in vec4 Vertex;
layout(location=3) in vec4 Color;

out Attribs {
    vec4 couleurAvant;
    vec4 couleurArriere;
} AttribsOut;

void main(void)
{
    gl_Position = matrProj * matrVisu * matrModel * Vertex;

    // assigner la couleur du sommet
    AttribsOut.couleurAvant = Color;
    AttribsOut.couleurArriere = 1.0 - Color;
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_FRAGMENTS)

in Attribs {
    vec4 couleurAvant;
    vec4 couleurArriere;
} AttribsIn;

out vec4 FragColor;

void main(void)
{
    // assigner la couleur du fragment qui est la couleur interpolée
    vec4 coul = gl_FrontFacing ? AttribsIn.couleurAvant : AttribsIn.couleurArriere;
    FragColor = coul;
}

#endif
